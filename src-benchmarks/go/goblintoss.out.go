package main

import (
	"fmt"
	"math"
	"os"
	"strconv"
	"time"
)

type Vector3 struct {
	x, y, z float64
}

func Vector3New(x float64, y float64, z float64) Vector3 {
	return Vector3 {
		x: x,
		y: y,
		z: z,
	}
}

func (v *Vector3) toString(unit string) string {
	return fmt.Sprintf("{ x: %.8f %s, y: %.8f %s, z: %.8f %s }", v.x, unit, v.y, unit, v.z, unit)
}

type Goblin struct {
	pos, vel Vector3
	radius, mass, dragCoef, frontA, restitution float64
}

func GoblinNew(pos Vector3, vel Vector3) Goblin {
	radius := 0.25
	return Goblin {
		pos: pos,
		vel: vel,
		radius: radius,
		mass: 18.0,
		dragCoef: 0.5,
		frontA: math.Pi * radius * radius,
		restitution: 0.8,
	}
}

type GoblinTossArena struct {
	bounceCount int32
	maxY, minY, maxX, minX, maxZ, minZ, gravityAcc, airDensity float64
	goblin Goblin
}

func GoblinTossArenaNew() GoblinTossArena {
	return GoblinTossArena {
		bounceCount: 0,
		maxY: 50.0,
		minY: 0.0,
		maxX: 25.0,
		minX: -25.0,
		maxZ: 25.0,
		minZ: -25.0,
		gravityAcc: 9.81,
		airDensity: 1.226,
		goblin: GoblinNew(Vector3New(0.0, 0.0, 0.0), Vector3New(0.0, 0.0, 0.0)),
	}
}

func (gta *GoblinTossArena) tossNewGoblin(startVel Vector3) {
	gta.goblin = GoblinNew(Vector3New(0.0, gta.minY + 2.0, 0.0), startVel)
	gta.bounceCount = 0
}

func (gta *GoblinTossArena) getDragForce(Cd float64, v float64, A float64) float64 {
	p := gta.airDensity
	velDir := -1.0
	if v > 0 {
		velDir = 1.0
	}
	return -0.5 * p * (v * v) * Cd * A * velDir;
}

func (gta *GoblinTossArena) step(seconds float64) Vector3 {
	g := &gta.goblin
	fdx := gta.getDragForce(g.dragCoef, g.vel.x, g.frontA)
	fdy := gta.getDragForce(g.dragCoef, g.vel.y, g.frontA)
	fdz := gta.getDragForce(g.dragCoef, g.vel.z, g.frontA)

	accx := fdx / g.mass
	accy := fdy / g.mass - gta.gravityAcc
	accz := fdz / g.mass

	g.vel.x += accx * seconds
	g.vel.y += accy * seconds
	g.vel.z += accz * seconds

	g.pos.x += g.vel.x * seconds
	g.pos.y += g.vel.y * seconds
	g.pos.z += g.vel.z * seconds

	gta.handleCollisions()
	return g.pos
}

func (gta *GoblinTossArena) handleCollisions() {
	g := &gta.goblin
	if g.pos.y - g.radius < gta.minY {
		g.vel.y *= -g.restitution
		g.pos.y = gta.minY + g.radius
		gta.bounceCount++
	}

	if g.pos.y + g.radius > gta.maxY {
		g.vel.y *= -g.restitution
		g.pos.y = gta.maxY - g.radius
	}

	if g.pos.x - g.radius < gta.minX {
		g.vel.x *= -g.restitution
		g.pos.x = gta.minX + g.radius
	}

	if g.pos.x + g.radius > gta.maxX {
		g.vel.x *= -g.restitution
		g.pos.x = gta.maxX - g.radius
	}

	if g.pos.z - g.radius < gta.minZ {
		g.vel.z *= -g.restitution
		g.pos.z = gta.minZ + g.radius
	}

	if g.pos.z + g.radius > gta.maxZ {
		g.vel.z *= -g.restitution
		g.pos.z = gta.maxZ - g.radius
	}
}

func main() {
	seconds := 10
	if len(os.Args) > 1 {
		argSecs, _ := strconv.Atoi(os.Args[1])
		seconds = argSecs
	}
	runBenchmark(seconds)
}

func runBenchmark(seconds int) {
	arena := GoblinTossArenaNew()
	step := 0.01
	xVels := []float64 { 70.0, 60.0, 50.0, 40.0, 30.0 }
	yVels := []float64 { 50.0, 40.0, 30.0 }
	zVels := []float64 { 60.0, 50.0 }
	goblinsTossed := 0

	startTime := time.Now()

	for time.Since(startTime) < time.Second * time.Duration(seconds) {
		yPosPrev := -1.0
		yPosCurr := 0.0
		startVx := xVels[goblinsTossed % len(xVels)]
		startVy := yVels[goblinsTossed % len(yVels)]
		startVz := zVels[goblinsTossed % len(zVels)]
		startVel := Vector3New(startVx, startVy, startVz)

		arena.tossNewGoblin(startVel)

		for yPosPrev != yPosCurr {
			yPosPrev = yPosCurr
			yPosCurr = arena.step(step).y
		}

		goblinsTossed++
	}

	fmt.Printf("Number of Goblins Tossed:%d\n", goblinsTossed)
}

func runValidation() {
	arena := GoblinTossArenaNew()
	step := 0.001
	stepCount := 0
	yPosPrev := -1.0
	yPosCurr := 0.0

	startVx := 50.0
	startVy := 30.0
	startVz := 20.0
	startVel := Vector3New(startVx, startVy, startVz)

	arena.tossNewGoblin(startVel)

	for yPosPrev != yPosCurr {
		yPosPrev = yPosCurr
		yPosCurr = arena.step(step).y
		stepCount++
	}

	startVel = Vector3New(startVx, startVy, startVz)
	fmt.Printf("steps: %d (%fs) | y-bounces: %d | vel-initial: %s | pos-final: %fm\n", stepCount, step, arena.bounceCount, startVel.toString("m/s"), yPosCurr)
}