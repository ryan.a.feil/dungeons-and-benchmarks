#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define PI 3.14159265358979323846

typedef struct Vector3 {
	double x;
	double y;
	double z;
} Vector3;

typedef struct Goblin {
	struct Vector3* pos;
	struct Vector3* vel;
	double radius, mass, dragCoef, frontA, restitution;
} Goblin;

typedef struct GoblinTossArena {
	int bounceCount;
	double maxY, minY, maxX, minX, maxZ, minZ, gravityAcc, airDensity;
	Goblin* goblin;
} GoblinTossArena;

void arena_handle_collision(GoblinTossArena* gta);

Vector3* vector3_new(double x, double y, double z) {
	Vector3* newVec = malloc(sizeof(Vector3));
	newVec->x = x;
	newVec->y = y;
	newVec->z = z;
	return newVec;
}

void vector3_free(Vector3* vec) {
	free(vec);
}

Goblin* goblin_new(Vector3* pos, Vector3* vel) {
	double radius = 0.25;
	Goblin* newGoblin = malloc(sizeof(Goblin));
	newGoblin->pos = pos;
	newGoblin->vel = vel;
	newGoblin->radius = radius;
	newGoblin->mass = 18.0;
	newGoblin->dragCoef = 0.5;
	newGoblin->frontA = PI * radius * radius;
	newGoblin->restitution = 0.8;
	return newGoblin;
}

void goblin_free(Goblin* goblin) {
	vector3_free(goblin->pos);
	vector3_free(goblin->vel);
	free(goblin);
}

GoblinTossArena* arena_new() {
	GoblinTossArena* newArena = malloc(sizeof(GoblinTossArena));
	newArena->bounceCount = 0;
	newArena->maxY = 50.0;
	newArena->minY = 0.0;
	newArena->maxX = 25.0;
	newArena->minX = -25.0;
	newArena->maxZ = 25.0;
	newArena->minZ = -25.0;
	newArena->gravityAcc = 9.81;
	newArena->airDensity = 1.226;
	newArena->goblin = goblin_new(
		vector3_new(0.0, 0.0, 0.0),
		vector3_new(0.0, 0.0, 0.0)
	);
	return newArena;
}

void arena_toss_new_goblin(GoblinTossArena* gta, Vector3* startVel) {
	gta->goblin = goblin_new(vector3_new(0.0, gta->minY + 2.0, 0.0), startVel);
	gta->bounceCount = 0;
}

double arena_get_drag_force(GoblinTossArena* gta, double Cd, double v, double A) {
	double p = gta->airDensity;
	return -0.5 * p * (v * v) * Cd * A * (v > 0 ? 1 : -1);
}

Vector3* arena_step(GoblinTossArena* gta, double seconds) {
	Goblin* g = gta->goblin;

	double fdx = arena_get_drag_force(gta, g->dragCoef, g->vel->x, g->frontA);
	double fdy = arena_get_drag_force(gta, g->dragCoef, g->vel->y, g->frontA);
	double fdz = arena_get_drag_force(gta, g->dragCoef, g->vel->z, g->frontA);

	double accx = fdx / g->mass;
	double accy = fdy / g->mass - gta->gravityAcc;
	double accz = fdz / g->mass;

	g->vel->x += accx * seconds;
	g->vel->y += accy * seconds;
	g->vel->z += accz * seconds;

	g->pos->x += g->vel->x * seconds;
	g->pos->y += g->vel->y * seconds;
	g->pos->z += g->vel->z * seconds;

	arena_handle_collision(gta);
	return g->pos;
}

void arena_handle_collision(GoblinTossArena* gta) {
	Goblin* g = gta->goblin;
	if (g->pos->y - g->radius < gta->minY) {
		g->vel->y *= -g->restitution;
		g->pos->y = gta->minY + g->radius;
		gta->bounceCount++;
	}

	if (g->pos->y + g->radius > gta->maxY) {
		g->vel->y *= -g->restitution;
		g->pos->y = gta->maxY - g->radius;
	}

	if (g->pos->x - g->radius < gta->minX) {
		g->vel->x *= -g->restitution;
		g->pos->x = gta->minX + g->radius;
	}

	if (g->pos->x + g->radius > gta->maxX) {
		g->vel->x *= -g->restitution;
		g->pos->x = gta->maxX - g->radius;
	}

	if (g->pos->z - g->radius < gta->minZ) {
		g->vel->z *= -g->restitution;
		g->pos->z = gta->minZ + g->radius;
	}

	if (g->pos->z + g->radius > gta->maxZ) {
		g->vel->z *= -g->restitution;
		g->pos->z = gta->maxZ - g->radius;
	}
}

void run_benchmark(int seconds) {
	GoblinTossArena* arena = arena_new();
	int timeLimit = seconds;
	double step = 0.01;

	double xVels[] = { 70.0, 60.0, 50.0, 40.0, 30.0 };
	double yVels[] = { 50.0, 40.0, 30.0 };
	double zVels[] = { 60.0, 50.0 };
	int goblinsTossed = 0;
	clock_t startTime = clock();

	while (((clock() - startTime) / CLOCKS_PER_SEC) < timeLimit) {
		double yPosPrev = -1.0;
		double yPosCurr = 0.0;
		double startVx = xVels[goblinsTossed % (sizeof xVels / sizeof xVels[0])];
		double startVy = yVels[goblinsTossed % (sizeof yVels / sizeof yVels[0])];
		double startVz = zVels[goblinsTossed % (sizeof zVels / sizeof zVels[0])];
		Vector3* startVel = vector3_new(startVx, startVy, startVz);

		arena_toss_new_goblin(arena, startVel);

		while (yPosPrev != yPosCurr) {
			yPosPrev = yPosCurr;
			yPosCurr = arena_step(arena, step)->y;
		}

		goblinsTossed++;
	}

	printf("Number of Goblins Tossed:%d\n", goblinsTossed);
}

void run_validation() {
	GoblinTossArena* arena = arena_new();
	double step = 0.001;
	int stepCount = 0;
	double yPosPrev = -1.0;
	double yPosCurr = 0.0;

	double startVx = 50.0;
	double startVy = 30.0;
	double startVz = 20.0;
	Vector3* startVel = vector3_new(startVx, startVy, startVz);

	arena_toss_new_goblin(arena, startVel);

	while (yPosPrev != yPosCurr) {
		yPosPrev = yPosCurr;
		yPosCurr = arena_step(arena, step)->y;
		stepCount++;
	}

	startVel = vector3_new(startVx, startVy, startVz);

	printf("steps: %d (%fs) | y-bounces: %d | vel-initial: %s | pos-final: %fm\n", stepCount, step, arena->bounceCount, "someday i will tostring", yPosCurr);
}

int main(int argc, char** argv) {
	int seconds = argc == 1 ? 10 : atoi(argv[1]);
	run_benchmark(seconds);
}
