const { performance } = require('perf_hooks');

class Vector3 {
	constructor(x, y, z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	toString(unit) {
		return `{ x: ${this.x.toFixed(8)} ${unit}, y: ${this.y.toFixed(8)} ${unit}, z: ${this.z.toFixed(8)} ${unit} }`;
	}
}

class Goblin {
	constructor(pos, vel) {
		this.pos = pos;
		this.vel = vel;
		this.radius =  0.25;
		this.mass = 18.0;
		this.dragCoef = 0.5;
		this.frontA = Math.PI * this.radius * this.radius
		this.restitution = 0.8
	}

	toString() {
		return `pos: ${this.pos.toString('m')} vel: ${this.vel.toString('m/s')}`;
	}
}

class GoblinTossArena {
	constructor() {
		this.bounceCount = 0;
		this.maxY = 50.0;
		this.minY = 0.0;
		this.maxX = 25.0;
		this.minX = -25.0;
		this.maxZ = 25.0;
		this.minZ = -25.0;
		this.gravityAcc = 9.81;
		this.airDensity = 1.226;
		this.goblin = {};
	}

	tossNewGoblin(startVel) {
		this.goblin = new Goblin(new Vector3(0.0, this.minY + 2.0, 0.0), startVel);
		this.bounceCount = 0;
	}

	getDragForce(Cd, v, A) {
		const p = this.airDensity;
		return -0.5 * p * (v * v) * Cd * A * (v > 0 ? 1 : -1);
	}

	step(seconds) {
		let g = this.goblin;

		const fdx = this.getDragForce(g.dragCoef, g.vel.x, g.frontA);
		const fdy = this.getDragForce(g.dragCoef, g.vel.y, g.frontA);
		const fdz = this.getDragForce(g.dragCoef, g.vel.z, g.frontA);

		const accx = fdx / g.mass;
		const accy = fdy / g.mass - this.gravityAcc;
		const accz = fdz / g.mass;

		g.vel.x += accx * seconds;
		g.vel.y += accy * seconds;
		g.vel.z += accz * seconds;

		g.pos.x += g.vel.x * seconds;
		g.pos.y += g.vel.y * seconds;
		g.pos.z += g.vel.z * seconds;

		this.handleCollisions();
		return g.pos;
	}

	handleCollisions() {
		let g = this.goblin;
		if (g.pos.y - g.radius < this.minY) {
			g.vel.y *= -g.restitution;
			g.pos.y = this.minY + g.radius;
			this.bounceCount++;
		}

		if (g.pos.y + g.radius > this.maxY) {
			g.vel.y *= -g.restitution;
			g.pos.y = this.maxY - g.radius;
		}

		if (g.pos.x - g.radius < this.minX) {
			g.vel.x *= -g.restitution;
			g.pos.x = this.minX + g.radius;
		}

		if (g.pos.x + g.radius > this.maxX) {
			g.vel.x *= -g.restitution;
			g.pos.x = this.maxX - g.radius;
		}

		if (g.pos.z - g.radius < this.minZ) {
			g.vel.z *= -g.restitution;
			g.pos.z = this.minZ + g.radius;
		}

		if (g.pos.z + g.radius > this.maxZ) {
			g.vel.z *= -g.restitution;
			g.pos.z = this.maxZ - g.radius;
		}
	}
}

class GoblinToss {
	static main() {
		const seconds = process.argv[2] ? Number(process.argv[2]) : 10;
		GoblinToss.runBenchmark(seconds);
		process.exit();
	}

	static runBenchmark(seconds) {
		const arena = new GoblinTossArena();
		const timeLimit = seconds * 1000;
		const step = 0.01;

		const xVels = [70.0, 60.0, 50.0, 40.0, 30.0];
		const yVels = [50.0, 40.0, 30.0];
		const zVels = [60.0, 50.0];
		let goblinsTossed = 0;

		let startTime = performance.now();
		while (performance.now() - startTime < timeLimit) {
			let yPosPrev = -1;
			let yPosCurr = 0;
			let startVx = xVels[goblinsTossed % xVels.length];
			let startVy = yVels[goblinsTossed % yVels.length];
			let startVz = zVels[goblinsTossed % zVels.length];
			let startVel = new Vector3(startVx, startVy, startVz);

			arena.tossNewGoblin(startVel);

			while (yPosPrev !== yPosCurr) {
				yPosPrev = yPosCurr;
				yPosCurr = arena.step(step).y;
			}
	
			goblinsTossed++;
		}
	
		let endtime = performance.now();
		console.log(`Number of Goblins tossed:${goblinsTossed}`);
	}

	static runValidation() {
		const arena = new GoblinTossArena();
		const step = 0.001;
		let stepCount = 0;
		let yPosPrev = -1;
		let yPosCurr = 0;

		let startVx = 50.0;
		let startVy = 30.0;
		let startVz = 20.0;
		let startVel = new Vector3(startVx, startVy, startVz);

		arena.tossNewGoblin(startVel);
		
		while (yPosPrev !== yPosCurr) {
			yPosPrev = yPosCurr;
			yPosCurr = arena.step(step).y;
			stepCount++;
		}

		startVel = new Vector3(startVx, startVy, startVz);
		console.log(`steps: ${stepCount} (${step}s) | y-bounces: ${arena.bounceCount} | vel-initial: ${startVel.toString('m/s')} | pos-final: ${yPosCurr}m`);
	}
}

GoblinToss.main();
