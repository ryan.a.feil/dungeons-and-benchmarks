final class Vector3 {
	public double x, y, z;
	public Vector3(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public String toString(String unit) {
		return String.format("{ x: %.8f %s, y: %.8f %s, z: %.8f %s }", x, unit, y, unit, z, unit);
	}
}

final class Goblin {
	public Vector3 pos;
	public Vector3 vel;
	public double radius = 0.25;
	public double mass = 18.0;
	public double dragCoef = 0.5;
	public double frontA =  Math.PI * this.radius * this.radius;
	public double restitution = 0.8;

	public Goblin(Vector3 pos, Vector3 vel) {
		this.pos = pos;
		this.vel = vel;
	}

	public String toString() {
		return "pos: " + pos.toString("m") + " vel: " + vel.toString("m/s");
	}
}

final class GoblinTossArena {
	public int bounceCount = 0;
	public double maxY = 50.0;
	public double minY = 0.0;
	public double maxX = 25.0;
	public double minX = -25.0;
	public double maxZ = 25.0;
	public double minZ = -25.0;
	private double gravityAcc = 9.81;
	private double airDensity = 1.226;
	private Goblin goblin;

	public void tossNewGoblin(Vector3 startVel) {
		goblin = new Goblin(new Vector3(0.0, minY + 2.0, 0.0), startVel);
		bounceCount = 0;
	}

	private double getDragForce(double Cd, double v, double A) {
		double p = airDensity;
		return -0.5 * p * (v * v) * Cd * A * (v > 0 ? 1 : -1);
	}

	public Vector3 step(double seconds) {
		Goblin g = goblin;

		double fdx = getDragForce(g.dragCoef, g.vel.x, g.frontA);
		double fdy = getDragForce(g.dragCoef, g.vel.y, g.frontA);
		double fdz = getDragForce(g.dragCoef, g.vel.z, g.frontA);

		double accx = fdx / g.mass;
		double accy = fdy / g.mass - gravityAcc;
		double accz = fdz / g.mass;

		g.vel.x += accx * seconds;
		g.vel.y += accy * seconds;
		g.vel.z += accz * seconds;

		g.pos.x += g.vel.x * seconds;
		g.pos.y += g.vel.y * seconds;
		g.pos.z += g.vel.z * seconds;

		handleBouncing();
		return g.pos;
	}

	private void handleBouncing() {
		Goblin g = goblin;
		if (g.pos.y - g.radius < minY) {
			g.vel.y *= -g.restitution;
			g.pos.y = minY + g.radius;
			bounceCount ++;
		}

		if (g.pos.y + g.radius > maxY) {
			g.vel.y *= -g.restitution;
			g.pos.y = maxY - g.radius;
		}

		if (g.pos.x - g.radius < minX) {
			g.vel.x *= -g.restitution;
			g.pos.x = minX + g.radius;
		}

		if (g.pos.x + g.radius > maxX) {
			g.vel.x *= -g.restitution;
			g.pos.x = maxX - g.radius;
		}

		if (g.pos.z - g.radius < minZ) {
			g.vel.z *= -g.restitution;
			g.pos.z = minZ + g.radius;
		}

		if (g.pos.z + g.radius > maxZ) {
			g.vel.z *= -g.restitution;
			g.pos.z = maxZ - g.radius;
		}
	}
}

final class GoblinToss {
	public static void main(String[] args) {
		int seconds = args.length > 0 ? Integer.parseInt(args[0]) : 10;
		GoblinToss.runBenchmark(seconds);
	}

	public static void runBenchmark(int seconds) {	
		GoblinTossArena arena = new GoblinTossArena();
		int timeLimit = seconds * 1000;
		double step = 0.01;

		double[] xVels = { 70.0, 60.0, 50.0, 40.0, 30.0 };
		double[] yVels = { 50.0, 40.0, 30.0 };
		double[] zVels = { 60.0, 50.0 };
		int goblinsTossed = 0;

		long startTime = System.currentTimeMillis();
		while (System.currentTimeMillis() - startTime < timeLimit) {
			double yPosPrev = -1;
			double yPosCurr = 0;
			double startVx = xVels[goblinsTossed % xVels.length];
			double startVy = yVels[goblinsTossed % yVels.length];
			double startVz = zVels[goblinsTossed % zVels.length];
			Vector3 startVel = new Vector3(startVx, startVy, startVz);

			arena.tossNewGoblin(startVel);

			while (yPosPrev != yPosCurr) {
				yPosPrev = yPosCurr;
				yPosCurr = arena.step(step).y;
			}
	
			goblinsTossed++;
		}
	
		String resultMessage = String.format("Number of Goblins Tossed:%d", goblinsTossed);
		System.out.print(resultMessage);
	}

	public static void runValidation() {
		GoblinTossArena arena = new GoblinTossArena();
		double step = 0.001;
		int stepCount = 0;
		double yPosPrev = -1;
		double yPosCurr = 0;

		double startVx = 50.0;
		double startVy = 30.0;
		double startVz = 20.0;
		Vector3 startVel = new Vector3(startVx, startVy, startVz);

		arena.tossNewGoblin(startVel);
		
		while (yPosPrev != yPosCurr) {
			yPosPrev = yPosCurr;
			yPosCurr = arena.step(step).y;
			stepCount++;
		}

		startVel = new Vector3(startVx, startVy, startVz);

		System.out.format("steps: %d (%fs) | y-bounces: %d | vel-initial: %s | pos-final: %fm\n", stepCount, step, arena.bounceCount, startVel.toString("m/s"), yPosCurr);
	}
}