import math
import time
import sys

class Vector3:
	def __init__(self, x, y, z):
		self.x = x
		self.y = y
		self.z = z

	def to_string(self, unit):
		return f"{{ x: {round(self.x, 8)} {unit}, y: {round(self.y, 8)} {unit}, z: {round(self.z, 8)} {unit} }}"

class Goblin:
	def __init__(self, pos, vel):
		self.pos = pos
		self.vel = vel
		self.radius = 0.25
		self.mass = 18.0
		self.drag_coef = 0.5
		self.front_a = math.pi * self.radius * self.radius
		self.restitution = 0.8

	def to_string(self):
		# For some reason, I can't put string literals in to_string /shrug
		m = "m"
		ms = "m/s"
		return f"pos: {self.pos.to_string(m)} vel: {self.vel.to_string(ms)}"

class GoblinTossArena:
	def __init__(self):
		self.bounce_count = 0
		self.max_y = 50.0
		self.min_y = 0.0
		self.max_x = 25.0
		self.min_x = -25.0
		self.max_z = 25.0
		self.min_z = -25.0
		self.gravity_acc = 9.81
		self.air_density = 1.226
		self.goblin = None

	def toss_new_goblin(self, start_vel):
		self.goblin = Goblin(Vector3(0.0, self.min_y + 2.0, 0.0), start_vel)
		self.bounce_count = 0

	def get_drag_force(self, Cd, v, A):
		p = self.air_density
		return -0.5 * p * (v * v) * Cd * A * (1 if v > 0 else -1)

	def step(self, seconds):
		g = self.goblin

		fdx = self.get_drag_force(g.drag_coef, g.vel.x, g.front_a)
		fdy = self.get_drag_force(g.drag_coef, g.vel.y, g.front_a)
		fdz = self.get_drag_force(g.drag_coef, g.vel.z, g.front_a)
		
		accx = fdx / g.mass
		accy = fdy / g.mass - self.gravity_acc
		accz = fdz / g.mass

		g.vel.x += accx * seconds
		g.vel.y += accy * seconds
		g.vel.z += accz * seconds

		g.pos.x += g.vel.x * seconds
		g.pos.y += g.vel.y * seconds
		g.pos.z += g.vel.z * seconds

		self.handle_collisions()
		return g.pos

	def handle_collisions(self):
		g = self.goblin
		if g.pos.y - g.radius < self.min_y:
			g.vel.y *= -g.restitution
			g.pos.y = self.min_y + g.radius
			self.bounce_count += 1

		if g.pos.y + g.radius > self.max_y:
			g.vel.y *= -g.restitution
			g.pos.y = self.max_y - g.radius

		if g.pos.x - g.radius < self.min_x:
			g.vel.x *= -g.restitution
			g.pos.x = self.min_y + g.radius

		if g.pos.x + g.radius > self.max_x:
			g.vel.x *= -g.restitution
			g.pos.x = self.max_x- g.radius

		if g.pos.z - g.radius < self.min_z:
			g.vel.z *= -g.restitution
			g.pos.z = self.min_z + g.radius

		if g.pos.z + g.radius > self.max_z:
			g.vel.z *= -g.restitution
			g.pos.z = self.max_z - g.radius

class GoblinToss:
	def main():
		seconds = 10 if len(sys.argv) <= 1 else int(sys.argv[1])
		GoblinToss.run_benchmark(seconds);

	def run_benchmark(seconds):
		arena = GoblinTossArena()
		time_limit = seconds
		step = 0.01
		
		x_vels = [70.0, 60.0, 50.0, 40.0, 30.0]
		y_vels = [50.0, 40.0, 30.0]
		z_vels = [60.0, 50.0]
		goblins_tossed = 0

		start_time = time.time()
		while time.time() - start_time < time_limit:
			y_pos_prev = -1
			y_pos_curr = 0
			start_vx = x_vels[goblins_tossed % len(x_vels)]
			start_vy = y_vels[goblins_tossed % len(y_vels)]
			start_vz = z_vels[goblins_tossed % len(z_vels)]
			start_vel = Vector3(start_vx, start_vy, start_vz)

			arena.toss_new_goblin(start_vel)

			while y_pos_prev != y_pos_curr:
				y_pos_prev = y_pos_curr
				y_pos_curr = arena.step(step).y
			
			goblins_tossed += 1
		
		print(f"Number of Goblins tossed:{goblins_tossed}")

	def run_validation():
		arena = GoblinTossArena()
		step = 0.001
		step_count = 0
		y_pos_prev = -1
		y_pos_curr = 0

		start_vx = 50.0
		start_vy = 30.0
		start_vz = 20.0
		start_vel = Vector3(start_vx, start_vy, start_vz);
		
		arena.toss_new_goblin(start_vel)
		
		while y_pos_prev != y_pos_curr:
			y_pos_prev = y_pos_curr
			y_pos_curr = arena.step(step).y
			step_count += 1

		start_vel = Vector3(start_vx, start_vy, start_vz);
		print(f"steps: {step_count} ({step}s) | y-bounces: {arena.bounce_count} | vel-initial: {start_vel.to_string('m/s')} | pos-final: {y_pos_curr}m")

GoblinToss.main()