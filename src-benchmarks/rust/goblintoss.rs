use std::env;
use std::time::Instant;
use std::f64::consts;

struct Vector3 {
	x: f64,
	y: f64,
	z: f64,
}

impl Vector3 {
	pub fn new(x: f64, y: f64, z: f64) -> Vector3 {
		Vector3 {
			x: x,
			y: y,
			z: z,
		}
	}

	pub fn to_string(&self, unit: &str) -> String {
		return format!("{{ x: {} {}, y: {} {}, z: {} {} }}", self.x, unit, self.y, unit, self.z, unit);
	}
}

struct Goblin {
	pos: Vector3,
	vel: Vector3,
	radius: f64,
	mass: f64,
	drag_coef: f64,
	front_a: f64,
	restitution: f64,
}

impl Goblin {
	pub fn new(pos: Vector3, vel: Vector3) -> Goblin {
		let radius = 0.25;
		Goblin {
			pos: pos,
			vel: vel,
			radius: radius,
			mass: 18.0,
			drag_coef: 0.5,
			front_a: consts::PI * radius * radius,
			restitution: 0.8,
		}
	}
}

struct GoblinTossArena {
	bounce_count: i32,
	max_y: f64,
	min_y: f64,
	max_x: f64,
	min_x: f64,
	max_z: f64,
	min_z: f64,
	gravity_acc: f64,
	air_density: f64,
	goblin: Goblin,
}

impl GoblinTossArena {
	pub fn new() -> GoblinTossArena {
		GoblinTossArena {
			bounce_count: 0,
			max_y: 50.0,
			min_y: 0.0,
			max_x: 25.0,
			min_x: -25.0,
			max_z: 25.0,
			min_z: -25.0,
			gravity_acc: 9.81,
			air_density: 1.226,
			goblin: Goblin::new(
				Vector3::new(0.0, 0.0, 0.0),
				Vector3::new(0.0, 0.0, 0.0),
			),
		}
	}

	pub fn toss_new_goblin(&mut self, start_vel: Vector3) {
		self.goblin = Goblin::new(Vector3::new(0.0, self.min_y + 2.0, 0.0), start_vel);
		self.bounce_count = 0;
	}

	pub fn get_drag_force(&self, cd: f64, v: f64, a: f64) -> f64 {
		let p = self.air_density;
		return -0.5 * p * (v * v) * cd * a * if v > 0.0 { 1.0 } else { -1.0 };
	}

	pub fn step(&mut self, seconds: f64) -> Vector3 {
		let fdx = self.get_drag_force(self.goblin.drag_coef, self.goblin.vel.x, self.goblin.front_a);
		let fdy = self.get_drag_force(self.goblin.drag_coef, self.goblin.vel.y, self.goblin.front_a);
		let fdz = self.get_drag_force(self.goblin.drag_coef, self.goblin.vel.z, self.goblin.front_a);

		let accx = fdx / self.goblin.mass;
		let accy = fdy / self.goblin.mass - self.gravity_acc;
		let accz = fdz / self.goblin.mass;

		self.goblin.vel.x += accx * seconds;
		self.goblin.vel.y += accy * seconds;
		self.goblin.vel.z += accz * seconds;

		self.goblin.pos.x += self.goblin.vel.x * seconds;
		self.goblin.pos.y += self.goblin.vel.y * seconds;
		self.goblin.pos.z += self.goblin.vel.z * seconds;

		self.handle_collisions();
		return Vector3::new(self.goblin.pos.x, self.goblin.pos.y, self.goblin.pos.z);
	}

	pub fn handle_collisions(&mut self) {
		if self.goblin.pos.y - self.goblin.radius < self.min_y {
			self.goblin.vel.y *= -self.goblin.restitution;
			self.goblin.pos.y = self.min_y + self.goblin.radius;
			self.bounce_count += 1;
		}

		if self.goblin.pos.y + self.goblin.radius > self.max_y {
			self.goblin.vel.y *= -self.goblin.restitution;
			self.goblin.pos.y = self.max_y - self.goblin.radius;
		}

		if self.goblin.pos.x - self.goblin.radius < self.min_x {
			self.goblin.vel.x *= -self.goblin.restitution;
			self.goblin.pos.x = self.min_y + self.goblin.radius;
		}

		if self.goblin.pos.x + self.goblin.radius > self.max_x {
			self.goblin.vel.x *= -self.goblin.restitution;
			self.goblin.pos.x = self.max_x- self.goblin.radius;
		}

		if self.goblin.pos.z - self.goblin.radius < self.min_z {
			self.goblin.vel.z *= -self.goblin.restitution;
			self.goblin.pos.z = self.min_z + self.goblin.radius;
		}

		if self.goblin.pos.z + self.goblin.radius > self.max_z {
			self.goblin.vel.z *= -self.goblin.restitution;
			self.goblin.pos.z = self.max_z - self.goblin.radius;
		}
	}
}

fn run_benchmark(seconds: u64) {
	let mut arena = GoblinTossArena::new();
	let step = 0.01;
	let x_vels = [70.0, 60.0, 50.0, 40.0, 30.0];
	let y_vels = [50.0, 40.0, 30.0];
	let z_vels = [60.0, 50.0];
	let mut goblins_tossed = 0;

	let start_time = Instant::now();

	while start_time.elapsed().as_secs() < seconds {
		let mut y_pos_prev = -1.0;
		let mut y_pos_curr = 0.0;
		let start_vx = x_vels[goblins_tossed % x_vels.len()];
		let start_vy = y_vels[goblins_tossed % y_vels.len()];
		let start_vz = z_vels[goblins_tossed % z_vels.len()];
		let start_vel = Vector3::new(start_vx, start_vy, start_vz);

		arena.toss_new_goblin(start_vel);

		while y_pos_prev != y_pos_curr {
			y_pos_prev = y_pos_curr;
			y_pos_curr = arena.step(step).y;
		}

		goblins_tossed += 1;
	}

	println!("Number of Goblins tossed:{}", goblins_tossed);
}

fn run_validation() {
	let mut arena = GoblinTossArena::new();
	let step = 0.001;
	let mut step_count = 0;
	let mut y_pos_prev = -1.0;
	let mut y_pos_curr = 0.0;

	let start_vx = 50.0;
	let start_vy = 30.0;
	let start_vz = 20.0;
	let mut start_vel = Vector3::new(start_vx, start_vy, start_vz);

	arena.toss_new_goblin(start_vel);

	while y_pos_prev != y_pos_curr {
		y_pos_prev = y_pos_curr;
		y_pos_curr = arena.step(step).y;
		step_count += 1;
	}

	start_vel = Vector3::new(start_vx, start_vy, start_vz);
	println!("steps: {} ({}s) | y-bounces: {} | vel-initial: {} | pos-final: {}m", step_count, step, arena.bounce_count, start_vel.to_string("m/s"), y_pos_curr);
}

fn main() {
	if let Some(sec_arg) = env::args().nth(1) {
		let sec_res: Result<u64, _> = sec_arg.parse();
		match sec_res {
			Ok(seconds) => run_benchmark(seconds),
			Err(_) => run_benchmark(10),
		}
	}
}