class Vector3
	property x, y, z
	def initialize(@x : Float64, @y : Float64, @z : Float64) end
	
	def to_s(unit : String) : String
		return "{ x: #{@x.round(8)} #{unit}, y: #{@y.round(8)} #{unit}, z: #{@z.round(8)} #{unit} }"
	end
end

class Goblin
	property pos, vel
	getter radius, mass, drag_coef, front_a, restitution
	
	@radius : Float64 = 0.25
	@mass : Float64 = 18.0
	@drag_coef : Float64 = 0.5
	@front_a : Float64 = Math::PI * @radius * @radius
	@restitution : Float64 = 0.8

	def initialize(@pos : Vector3, @vel : Vector3) end

	def to_s() : String
		return "pos: #{@pos.to_s("m")} vel: #{@vel.to_s("ms")}"
	end
end

class GoblinTossArena
	getter bounce_count
	@bounce_count : UInt32 = 0_u32
	@max_y : Float64 = 50.0
	@min_y : Float64 = 0.0
	@max_x : Float64 = 25.0
	@min_x : Float64 = -25.0
	@max_z : Float64 = 25.0
	@min_z : Float64 = -25.0
	@gravity_acc : Float64 = 9.81
	@air_density : Float64 = 1.226
	@goblin : Goblin

	def initialize()
		@goblin = Goblin.new(Vector3.new(0.0, 0.0, 0.0), Vector3.new(0.0, 0.0, 0.0))
	end

	def toss_new_goblin(start_vel : Vector3) : Nil
		@goblin = Goblin.new(Vector3.new(0.0, @min_y + 2.0, 0.0), start_vel)
		@bounce_count = 0_u32
	end

	def get_drag_force(cd : Float64, v : Float64, a : Float64) : Float64
		p = @air_density
		return -0.5 * p * (v * v) * cd * a * (v > 0.0 ? 1.0 : -1.0)
	end

	def step(seconds : Float64) : Vector3
		g = @goblin
		
		fdx = self.get_drag_force(g.drag_coef, g.vel.x, g.front_a)
		fdy = self.get_drag_force(g.drag_coef, g.vel.y, g.front_a)
		fdz = self.get_drag_force(g.drag_coef, g.vel.z, g.front_a)
		
		accx = fdx / g.mass
		accy = fdy / g.mass - @gravity_acc
		accz = fdz / g.mass

		g.vel.x += accx * seconds
		g.vel.y += accy * seconds
		g.vel.z += accz * seconds

		g.pos.x += g.vel.x * seconds
		g.pos.y += g.vel.y * seconds
		g.pos.z += g.vel.z * seconds

		self.handle_collisions()
		return g.pos
	end

	def handle_collisions()
		g = @goblin
		if g.pos.y - g.radius < @min_y
			g.vel.y *= -g.restitution
			g.pos.y = @min_y + g.radius
			@bounce_count += 1
		end

		if g.pos.y + g.radius > @max_y
			g.vel.y *= -g.restitution
			g.pos.y = @max_y - g.radius
		end

		if g.pos.x - g.radius < @min_x
			g.vel.x *= -g.restitution
			g.pos.x = @min_y + g.radius
		end

		if g.pos.x + g.radius > @max_x
			g.vel.x *= -g.restitution
			g.pos.x = @max_x- g.radius
		end

		if g.pos.z - g.radius < @min_z
			g.vel.z *= -g.restitution
			g.pos.z = @min_z + g.radius
		end

		if g.pos.z + g.radius > @max_z
			g.vel.z *= -g.restitution
			g.pos.z = @max_z - g.radius
		end
	end
end

class GoblinToss
	def self.main() : Nil
		seconds = ARGV.size == 0 ? 10 : ARGV[0].to_i32()
		run_benchmark(seconds)
	end

	def self.run_benchmark(seconds : Int32) : Nil
		arena = GoblinTossArena.new()
		time_limit = seconds * 1000
		step = 0.01
		
		x_vels = [70.0, 60.0, 50.0, 40.0, 30.0]
		y_vels = [50.0, 40.0, 30.0]
		z_vels = [60.0, 50.0]
		goblins_tossed = 0

		start_time = Time.new.epoch_ms()
		while Time.new.epoch_ms() - start_time < time_limit
			y_pos_prev = -1.0
			y_pos_curr = 0.0
			start_vx = x_vels[goblins_tossed % x_vels.size]
			start_vy = y_vels[goblins_tossed % y_vels.size]
			start_vz = z_vels[goblins_tossed % z_vels.size]
			start_vel = Vector3.new(start_vx, start_vy, start_vz)

			arena.toss_new_goblin(start_vel)

			while y_pos_prev != y_pos_curr
				y_pos_prev = y_pos_curr
				y_pos_curr = arena.step(step).y
			end

			goblins_tossed += 1
		end

		puts("Number of Goblins tossed:#{goblins_tossed}")
	end

	def self.run_validation() : Nil
		arena = GoblinTossArena.new()
		step = 0.001
		step_count = 0
		y_pos_prev = -1.0
		y_pos_curr = 0.0

		start_vx = 50.0
		start_vy = 30.0
		start_vz = 20.0
		start_vel = Vector3.new(start_vx, start_vy, start_vz);
		
		arena.toss_new_goblin(start_vel)

		while y_pos_prev != y_pos_curr
			y_pos_prev = y_pos_curr
			y_pos_curr = arena.step(step).y
			step_count += 1
		end

		start_vel = Vector3.new(start_vx, start_vy, start_vz);

		puts("steps: #{step_count} (#{step}s) | y-bounces: #{arena.bounce_count} | vel-initial: #{start_vel.to_s("m/s")} | pos-final: #{y_pos_curr}m")
	end
end

GoblinToss.main()