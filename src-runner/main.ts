import BenchRunner from './benchRunner';
import { benchConfigs } from './benchConfigs';

const main = async () => {
	const runner = new BenchRunner(benchConfigs, 1);
	await runner.start();
};

main();
