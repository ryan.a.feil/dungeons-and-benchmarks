import * as pidusage from 'pidusage';
import { BenchResult, BenchConfig } from './types';

export default class BenchStatCollector {
	private active: boolean;
	private benchConfig: BenchConfig;
	private measurements:any[];
	private memoryAvg: number;
	private memoryMax: number;
	private pid: number;
	private statInterval: any;

	constructor(benchConfig: BenchConfig, pid: number) {
		this.active = false;
		this.benchConfig = benchConfig;
		this.measurements = [];
		this.memoryAvg = 0;
		this.memoryMax = 0;
		this.pid = pid;
		this.statInterval = null;
	}

	start(): void {
		this.active = true;
		this.startStatInterval();
	}

	stop(): void {
		this.active = false;
		this.stopStatInterval();

		if (this.measurements && this.measurements.length > 0) {
			this.memoryAvg = this.measurements.reduce((p, c) => p + c) / this.measurements.length;
		}
	}

	result(): BenchResult {
		if (this.active) { this.stop(); }

		return {
			memoryAvg: this.memoryAvg,
			memoryMax: this.memoryMax,
			language: this.benchConfig.language,
			benchmark: this.benchConfig.benchmark,
			score: -1,
		};
	}

	startStatInterval(): void {
		const collectStats = async () => {
			try {
				const stats = await pidusage(this.pid);
				const memInMegabytes = stats.memory / 1000000;
				if (memInMegabytes > this.memoryMax) {
					this.memoryMax = memInMegabytes;
				}
				this.measurements.push(memInMegabytes);
			} catch (err) {
				console.log(err);
				clearInterval(this.statInterval);
			}
		};

		this.statInterval = setInterval(collectStats, 200);
	}

	stopStatInterval(): void {
		clearInterval(this.statInterval);
	}
}
