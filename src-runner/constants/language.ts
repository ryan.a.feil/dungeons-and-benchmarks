export enum Language {
	c = 'C',
	crystal = 'Crystal',
	go = 'Go',
	java = 'Java',
	nodejs = 'NodeJS',
	python3 = 'Python',
	rust = 'Rust',
}
