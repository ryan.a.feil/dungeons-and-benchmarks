import { Benchmark, Language } from '../constants';

export interface BenchConfig {
	language: Language;
	benchmark: Benchmark;
	compileCmd: string | null;
	compileArgs: string[] | null;
	runCmd: string;
	runArgs: string[] | null;
}
