import { Benchmark, Language } from '../constants';

export interface BenchResult {
	memoryMax: number;
	memoryAvg: number;
	language: Language;
	benchmark: Benchmark;
	score: number;
}
