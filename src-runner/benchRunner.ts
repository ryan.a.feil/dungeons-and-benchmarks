import * as Table from 'cli-table2';
import { spawn } from 'child_process';
import { BenchConfig, BenchResult } from './types';
import BenchStatCollector from './benchStatCollector';

export default class BenchRunner {
	constructor(private benchConfigs: BenchConfig[], private iterations: number) { }

	async start(): Promise<void> {
		console.log('Now starting the benchmark run!');
		console.log(`Each of these ${this.benchConfigs.length} benchmarks will be run ${this.iterations} times.\n`);
		await this.compileAllBenchmarks();
		const results = await this.runAllBenchmarks();

		const resultsTable = new Table({
			head: ['Lang', 'Bench', 'Score', 'Mem (avg)', 'Mem (max)'],
		}) as any;

		results.forEach(result => resultsTable.push([result.language, result.benchmark, result.score, result.memoryAvg.toFixed(2), result.memoryMax.toFixed(2)]));
		console.log('\nRESULTS');
		console.log(resultsTable.toString());
	}

	private async runAllBenchmarks(): Promise<BenchResult[]> {
		const results = [];
		for (let i = 1; i <= this.iterations; i += 1) {
			for (const benchmark of this.benchConfigs) {
				try {
					const result = await this.runBenchmark(benchmark);
					results.push(result);
					console.log(`The "${benchmark.benchmark}" benchmark has been run for ${benchmark.language} ${i} of ${this.iterations} times.`);
				} catch (err) {
					console.log(`The "${benchmark.benchmark}" benchmark couldn\'t run for ${benchmark.language}. Check if "${benchmark.runCmd}" is installed.`);
				}
			}
		}
		return results;
	}

	private async runBenchmark(benchConfig: BenchConfig): Promise<BenchResult> {
		return new Promise<BenchResult>((resolve, reject) => {
			const benchProcess = benchConfig.runArgs != null
				? spawn(benchConfig.runCmd, benchConfig.runArgs)
				: spawn(benchConfig.runCmd);

			const statCollector = new BenchStatCollector(benchConfig, benchProcess.pid);
			statCollector.start();

			benchProcess.on('exit', () => {
				statCollector.stop();
			});

			benchProcess.stdout.on('data', (data) => {
				statCollector.stop();
				const result = statCollector.result();
				const scoreString = data.toString();
				const score = Number(scoreString.split(':')[1]);
				result.score = score;
				resolve(result);
			});

			benchProcess.on('error', (err: any) => reject(err));
		});
	}

	private async compileAllBenchmarks(): Promise<void> {
		const compileableBenches = this.benchConfigs.filter(b => b.compileCmd != null);

		for (const b of compileableBenches) {
			try {
				await this.compileBenchmark(b);
			} catch (err) {
				console.log(`The "${b.benchmark}" benchmark couldn\'t be compiled for ${b.language}. Check if "${b.compileCmd}" is installed.`);
			}
		}
	}

	private async compileBenchmark(benchmark: BenchConfig): Promise<void> {
		return new Promise<void>((resolve, reject) => {
			if (!benchmark.compileCmd && !benchmark.compileArgs) {
				resolve();
			} else {
				const compileProcess = benchmark.compileArgs
					? spawn(benchmark.compileCmd, benchmark.compileArgs)
					: spawn(benchmark.compileCmd);

				compileProcess.on('exit', () => {
					resolve();
				});

				compileProcess.on('error', (err: any) => reject(err));
			}
		});
	}
}
