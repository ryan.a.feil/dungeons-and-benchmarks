import { BenchConfig } from './types';
import { Benchmark, Language } from './constants';

const timeLimit = 5;

export const benchConfigs: BenchConfig[] = [
	{
		language: Language.c,
		benchmark: Benchmark.goblinToss,
		compileCmd: 'gcc',
		compileArgs: ['./src-benchmarks/c/goblintoss.c', '-o', './src-benchmarks/c/goblintoss.out', '-O3'],
		runCmd: './src-benchmarks/crystal/goblintoss.out',
		runArgs: [timeLimit.toString()],
	},
	{
		language: Language.crystal,
		benchmark: Benchmark.goblinToss,
		compileCmd: 'crystal',
		compileArgs: ['build', '--release', './src-benchmarks/crystal/goblintoss.cr', '-o', './src-benchmarks/crystal/goblintoss.out'],
		runCmd: './src-benchmarks/crystal/goblintoss.out',
		runArgs: [timeLimit.toString()],
	},
	{
		language: Language.go,
		benchmark: Benchmark.goblinToss,
		compileCmd: 'go',
		compileArgs: ['build', './src-benchmarks/go/goblintoss.out.go'],
		runCmd: './goblintoss.out',
		runArgs: [timeLimit.toString()],
	},
	{
		language: Language.java,
		benchmark: Benchmark.goblinToss,
		compileCmd: 'javac',
		compileArgs: ['./src-benchmarks/java/goblintoss.java'],
		runCmd: 'java',
		runArgs: ['-cp', './src-benchmarks/java', 'GoblinToss', timeLimit.toString()],
	},
	{
		language: Language.nodejs,
		benchmark: Benchmark.goblinToss,
		compileCmd: null,
		compileArgs: null,
		runCmd: 'node',
		runArgs: ['./src-benchmarks/nodejs/goblintoss.js', timeLimit.toString()],
	},
	{
		language: Language.python3,
		benchmark: Benchmark.goblinToss,
		compileCmd: null,
		compileArgs: null,
		runCmd: 'python3',
		runArgs: ['./src-benchmarks/python/goblintoss.py', timeLimit.toString()],
	},
	{
		language: Language.rust,
		benchmark: Benchmark.goblinToss,
		compileCmd: 'rustc',
		compileArgs: ['./src-benchmarks/rust/goblintoss.rs', '-o', './src-benchmarks/rust/goblintoss.out', '-C', 'opt-level=3'],
		runCmd: './src-benchmarks/rust/goblintoss.out',
		runArgs: [timeLimit.toString()],
	},
];
